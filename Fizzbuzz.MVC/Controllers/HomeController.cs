﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fizzbuzz.BusinessLogic.Interfaces;
using Fizzbuzz.MVC.Models;
using PagedList;

namespace Fizzbuzz.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzbuzzLogic _fizzbuzzLogic = null;
        private readonly IDateTimeProvider _dateTimeProvider = null;
        public HomeController(IFizzbuzzLogic FizzbuzzLogic, IDateTimeProvider DateTimeProvider)
        {
            _fizzbuzzLogic = FizzbuzzLogic;
            _dateTimeProvider = DateTimeProvider;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Result(FizzbuzzModel fizzbuzzModel, int? page)
        {
            int QueryNumber = Int32.Parse(fizzbuzzModel.inputString);
            DayOfWeek CurrentDay = _dateTimeProvider.GetCurrentDay();
            int pageNumber = page ?? 1;
            fizzbuzzModel.outputList = (PagedList<string>)_fizzbuzzLogic.GetFizzBuzzList(QueryNumber, CurrentDay).ToPagedList(pageNumber, 10);

            return View(fizzbuzzModel);
        }

        [HttpGet]
        public ActionResult Result(string inputString, int? page)
        {
            if (inputString == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int QueryNumber = Int32.Parse(inputString);
            FizzbuzzModel fizzbuzzModel = new FizzbuzzModel
            {
                inputString = inputString
            };
            DayOfWeek CurrentDay = _dateTimeProvider.GetCurrentDay();
            int pageNumber = page ?? 1;
            fizzbuzzModel.outputList = (PagedList<string>)_fizzbuzzLogic.GetFizzBuzzList(QueryNumber, CurrentDay).ToPagedList(pageNumber, 10);

            return View(fizzbuzzModel);
        }
    }
}