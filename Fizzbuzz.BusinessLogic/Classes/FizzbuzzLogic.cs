﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fizzbuzz.BusinessLogic.Interfaces;


namespace Fizzbuzz.BusinessLogic.Classes
{
    public class FizzbuzzLogic : IFizzbuzzLogic
    {
        private IEnumerable<IRule> _rules;
        private IDateTimeProvider _dateTimeProvider;

        public FizzbuzzLogic(IEnumerable<IRule> rules, IDateTimeProvider dateTimeProvider)
        {
            this._rules = rules;
            this._dateTimeProvider = dateTimeProvider;
        }

        public List<string> GetFizzBuzzList(int QueryNumber, DayOfWeek CurrentDay)
        {
            List<string> FizzbuzzList = new List<string>(QueryNumber);

            Enumerable.Range(1, QueryNumber).ToList().ForEach(index =>
            {
                var FizzbuzzString = new StringBuilder();

                this._rules.Where(rule => rule.IsMultiple(index)).ToList().ForEach(rule => FizzbuzzString.Append("").Append(rule.GetString(CurrentDay)));

                FizzbuzzList.Add(FizzbuzzString.Length > 0 ? FizzbuzzString.ToString() : index.ToString());
            });

            return FizzbuzzList;
        }
    }
}
