﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fizzbuzz.BusinessLogic.Interfaces;


namespace Fizzbuzz.BusinessLogic.Classes
{
    public class MultipleOfFive : IRule
    {
        public bool IsMultiple(int Number)
        {
            return (Number % 5 == 0);
        }

        public string GetString(DayOfWeek CurrentDay)
        {
            if (CurrentDay.Equals(DayOfWeek.Wednesday))
            {
                return "Wuzz";
            }
            else
            {
                return "Buzz";
            }
        }
    }
}
