﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fizzbuzz.BusinessLogic.Interfaces;

namespace Fizzbuzz.BusinessLogic.Classes
{
    public class MultipleOfThree : IRule
    {
        public bool IsMultiple(int Number)
        {
            return (Number % 3 == 0);
        }

        public string GetString(DayOfWeek CurrentDay)
        {
            if (CurrentDay.Equals(DayOfWeek.Wednesday))
            {
                return "Wizz";
            }
            else
            {
                return "Fizz";
            }
        }
    }
}
