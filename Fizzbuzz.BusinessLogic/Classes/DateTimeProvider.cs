﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fizzbuzz.BusinessLogic.Interfaces;

namespace Fizzbuzz.BusinessLogic.Classes
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DayOfWeek GetCurrentDay()
        {
            return DateTime.Today.DayOfWeek;
        }
    }
}
