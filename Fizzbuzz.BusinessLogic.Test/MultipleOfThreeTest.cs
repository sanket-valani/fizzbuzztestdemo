﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Fizzbuzz.BusinessLogic.Classes;

namespace Fizzbuzz.BusinessLogic.Test
{
    class MultipleOfThreeTest
    {

        [Test]
        public void IsMultiple_InputIsMultipleOfThree_ReturnsTrue()
        {
            // Arrange
            var MultipleOfThree = new MultipleOfThree();

            // Act
            bool Result = MultipleOfThree.IsMultiple(3);

            // Assert
            Assert.IsTrue(Result);
        }

        [Test]
        public void IsMultiple_InputIsNotMultipleOfThree_ReturnsFalse()
        {
            // Arrange
            var MultipleOfThree = new MultipleOfThree();

            // Act
            bool Result = MultipleOfThree.IsMultiple(5);

            // Assert
            Assert.IsFalse(Result);
        }

        [Test]
        public void GetString_CurrentDayIsWednesday_ReturnsWizz()
        {
            // Arrange
            var MultipleOfThree = new MultipleOfThree();

            // Act
            var Result = MultipleOfThree.GetString( System.DayOfWeek.Wednesday );

            // Assert
            var Expected = "Wizz";
            Assert.AreEqual(Expected,Result);
        }

        [Test]
        public void GetString_CurrentDayIsNotWednesday_ReturnsFizz()
        {
            // Arrange
            var MultipleOfThree = new MultipleOfThree();

            // Act
            var Result = MultipleOfThree.GetString(System.DayOfWeek.Monday);

            // Assert
            var Expected = "Fizz";
            Assert.AreEqual(Expected, Result);
        }
    }
}
