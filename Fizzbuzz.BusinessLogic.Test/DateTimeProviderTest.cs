﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Fizzbuzz.BusinessLogic.Classes;

namespace Fizzbuzz.BusinessLogic.Test
{
    class DateTimeProviderTest
    {
        [Test]
        public void GetCurrentDay_CheckForCurrentDay_ReturnsCurrentDay()
        {
            // Arrange
            var DateTimeProvider = new DateTimeProvider();

            // Act
            var Result = DateTimeProvider.GetCurrentDay();

            // Assert
            var Expected = System.DateTime.Today.DayOfWeek;
            Assert.AreEqual(Expected, Result);

        }
    }
}
