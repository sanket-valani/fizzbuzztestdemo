﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Fizzbuzz.BusinessLogic.Classes;

namespace Fizzbuzz.BusinessLogic.Test
{
    class MultipleOfFiveTest
    {

        [Test]
        public void IsMultiple_InputIsMultipleOfFive_ReturnsTrue()
        {
            // Arrange
            var MultipleOfFive = new MultipleOfFive();

            // Act
            bool Result = MultipleOfFive.IsMultiple(5);

            // Assert
            Assert.IsTrue(Result);
        }

        [Test]
        public void IsMultiple_InputIsNotMultipleOfFive_ReturnsFalse()
        {
            // Arrange
            var MultipleOfFive = new MultipleOfFive();

            // Act
            bool Result = MultipleOfFive.IsMultiple(6);

            // Assert
            Assert.IsFalse(Result);
        }

        [Test]
        public void GetString_CurrentDayIsWednesday_ReturnsWuzz()
        {
            // Arrange
            var MultipleOfFive = new MultipleOfFive();

            // Act
            var Result = MultipleOfFive.GetString(System.DayOfWeek.Wednesday);

            // Assert
            var Expected = "Wuzz";
            Assert.AreEqual(Expected, Result);
        }

        [Test]
        public void GetString_CurrentDayIsNotWednesday_ReturnsBuzz()
        {
            // Arrange
            var MultipleOfFive = new MultipleOfFive();

            // Act
            var Result = MultipleOfFive.GetString(System.DayOfWeek.Monday);

            // Assert
            var Expected = "Buzz";
            Assert.AreEqual(Expected, Result);
        }

    }
}
